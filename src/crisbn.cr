require "./crisbn/generate"

module ISBN
  VERSION = "0.3.5" # Still Unstable

  # Currently is inconsistent with the output number, will have to go back and refactor.

  # For Testing
  puts ISBN::Generate.english()
end
